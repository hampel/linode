<?php namespace Hampel\Linode;

use Mockery;
use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\Mock;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Exception\RequestException;

class LinodeServiceTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->mock = new Mock();

		$this->client = new Client();
		$this->client->getEmitter()->attach($this->mock);

		$this->command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
		$this->command->shouldReceive('getAction')->andReturn('test.echo');
		$this->command->shouldReceive('build')->andReturn(['api_action' => 'test.echo']);
	}

	protected function getMockPath()
	{
		return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
	}

	public function testException()
	{
		$this->mock->addException(new RequestException('foo', new Request('GET', 'http://example.com')));

		$this->setExpectedException('Hampel\Linode\Exception\LinodeRequestException', 'Error requesting Linode command [test.echo]: foo');

		$linode = new Linode($this->client);
		$linode->execute($this->command);
	}

	public function testServerError()
	{
		$this->mock->addResponse(new Response(500));

		$this->setExpectedException('Hampel\Linode\Exception\LinodeRequestException', 'Error requesting Linode command [test.echo]: Server error response [url] ?api_action=test.echo [status code] 500 [reason phrase] Internal Server Error');

		$linode = new Linode($this->client);
		$linode->execute($this->command);
	}

	public function testBadStatusReturned()
	{
		$this->mock->addResponse($this->getMockPath() . '500.json');

		$this->setExpectedException('Hampel\Linode\Exception\LinodeRequestException', 'Error requesting Linode command [test.echo]: Server error response [url] ?api_action=test.echo [status code] 503 [reason phrase] Service Unavailable');

		$linode = new Linode($this->client);
		$linode->execute($this->command);
	}

	public function testNoDataReturned()
	{
		$this->mock->addResponse($this->getMockPath() . 'empty.json');

		$this->setExpectedException('Hampel\Linode\Exception\LinodeDataException', 'Empty body received from command [test.echo]');

		$linode = new Linode($this->client);
		$linode->execute($this->command);
	}

	public function testInvalidJson()
	{
		$this->mock->addResponse($this->getMockPath() . 'invalid.json');

		$this->setExpectedException('Hampel\Linode\Exception\LinodeParseException', 'Error decoding JSON data returned from Linode command [test.echo]: Unable to parse JSON data: JSON_ERROR_SYNTAX - Syntax error, malformed JSON');

		$linode = new Linode($this->client);
		$linode->execute($this->command);
	}

	public function testInvalidResponse1()
	{
		$this->mock->addResponse($this->getMockPath() . 'invalid_response_1.json');

		$this->setExpectedException('Hampel\Linode\Exception\LinodeDataException', 'Invalid data received from command [test.echo] - no ERRORARRAY');

		$linode = new Linode($this->client);
		$linode->execute($this->command);
	}

	public function testInvalidResponse2()
	{
		$this->mock->addResponse($this->getMockPath() . 'invalid_response_2.json');

		$this->setExpectedException('Hampel\Linode\Exception\LinodeDataException', 'Invalid data received from command [test.echo] - no DATA');

		$linode = new Linode($this->client);
		$linode->execute($this->command);
	}

	public function testInvalidResponse3()
	{
		$this->mock->addResponse($this->getMockPath() . 'invalid_response_3.json');

		$this->setExpectedException('Hampel\Linode\Exception\LinodeDataException', 'Invalid data received from command [test.echo] - no ACTION');

		$linode = new Linode($this->client);
		$linode->execute($this->command);
	}

	public function testAuthFail()
	{
		$this->mock->addResponse($this->getMockPath() . 'auth_failed_4.json');

		$this->setExpectedException('Hampel\Linode\Exception\LinodeErrorException', 'Error processing Linode command [test.echo]: [4] Authentication failed');

		$linode = new Linode($this->client);
		$linode->execute($this->command);
	}

	public function testEcho()
	{
		$command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
		$command->shouldReceive('getAction')->andReturn('test.echo');
		$command->shouldReceive('build')->andReturn(['api_action' => 'test.echo', 'foo' => 'bar']);

		$this->mock->addResponse($this->getMockPath() . 'test_echo.json');

		$linode = new Linode($this->client);

		$response = $linode->execute($command);

		$this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
		$this->assertEquals('?api_action=test.echo&foo=bar', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertArrayHasKey('foo', $response);
		$this->assertEquals('bar', $response['foo']);
	}

    public function tearDown()
    {
        Mockery::close();
    }
}
