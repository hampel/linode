<?php namespace Hampel\Linode\Commands;

class CommandTest extends \PHPUnit_Framework_TestCase
{
	public function testInvalidActionEmpty()
	{
		$this->setExpectedException('Hampel\Linode\Exception\LinodeCommandException', 'Invalid action [] for [DomainCommand]');

		$command = new DomainCommand('');
	}

	public function testInvalidActionNull()
	{
		$this->setExpectedException('Hampel\Linode\Exception\LinodeCommandException', 'Invalid action [] for [DomainCommand]');

		$command = new DomainCommand(null);
	}

	public function testInvalidActionZero()
	{
		$this->setExpectedException('Hampel\Linode\Exception\LinodeCommandException', 'Invalid action [0] for [DomainCommand]');

		$command = new DomainCommand(0);
	}

	public function testInvalidActionOther()
	{
		$this->setExpectedException('Hampel\Linode\Exception\LinodeCommandException', 'Invalid action [foo] for [DomainCommand]');

		$command = new DomainCommand('foo');
	}


	public function testInvalidOptionNumericKey()
	{
		$this->setExpectedException('Hampel\Linode\Exception\LinodeCommandException', 'Invalid option key [0] for [DomainCommand]');

		$command = new DomainCommand('create', ['foo']);
	}

	public function testInvalidOptionBadKey()
	{
		$this->setExpectedException('Hampel\Linode\Exception\LinodeCommandException', 'Invalid option key [foo] for [DomainCommand]');

		$command = new DomainCommand('create', ['foo' => 'bar']);
	}

	public function testConstruction()
	{
		$command = new DomainCommand('create');

		$this->assertEquals('domain.create', $command->getAction());

		$query = $command->build();

		$this->assertTrue(is_array($query));
		$this->assertArrayHasKey('api_action', $query);
		$this->assertEquals('domain.create', $query['api_action']);

		$command = new DomainCommand('create', []);

		$this->assertEquals('domain.create', $command->getAction());

		$query = $command->build('foo');

		$this->assertTrue(is_array($query));
		$this->assertArrayHasKey('api_action', $query);
		$this->assertEquals('domain.create', $query['api_action']);
		$this->assertArrayHasKey('api_key', $query);
		$this->assertEquals('foo', $query['api_key']);

		$command = new DomainCommand('create', ['domain' => 'bar']);

		$this->assertEquals('domain.create', $command->getAction());

		$query = $command->build('foo');

		$this->assertTrue(is_array($query));
		$this->assertArrayHasKey('api_action', $query);
		$this->assertEquals('domain.create', $query['api_action']);
		$this->assertArrayHasKey('api_key', $query);
		$this->assertEquals('foo', $query['api_key']);
		$this->assertArrayHasKey('domain', $query);
		$this->assertEquals('bar', $query['domain']);
	}
}
