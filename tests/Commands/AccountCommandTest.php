<?php namespace Hampel\Linode\Commands;

use Mockery;
use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class AccountCommandTest extends \PHPUnit_Framework_TestCase
{
    protected $linode;

    public function setUp()
    {
        date_default_timezone_set('UTC');
        $this->linode = Linode::make(API_KEY);

        $this->mock = new Mock();

        $this->client = new Client();
        $this->client->getEmitter()->attach($this->mock);
    }

    /**
     * Where we store sample JSON responses.
     * @return string
     */
    protected function getMockPath()
    {
        return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
    }

    /**
     * @group network
     */
    public function testEstimateInvoice()
    {
        $response = $this->linode->execute(new AccountCommand('estimateinvoice', [
            'mode' => 'linode_new',
            'planid' => 2,
            'paymentterm' => 1
        ]));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
        $this->assertArrayHasKey('INVOICE_TO', $response);
        $this->assertArrayHasKey('AMOUNT', $response);
    }

    /**
     * @group network
     */
    public function testExceptionEstimateInvoice()
    {
        $this->setExpectedException('Hampel\Linode\Exception\LinodeErrorException', 'Error processing Linode command [account.estimateinvoice]');

        $response = $this->linode->execute(new AccountCommand('estimateinvoice', [
            'mode' => 'invalid mode'
        ]));
    }

    /**
     * Mock...
     */
    public function testMockEstimateInvoice()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('account.estimateinvoice');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'account.estimateinvoice',
            'mode' => 'linode_new',
            'planid' => 2,
            'paymentterm' => 1
        ]);

        $this->mock->addResponse($this->getMockPath() . 'account_estimateinvoice.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=account.estimateinvoice&mode=linode_new&planid=2&paymentterm=1', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('INVOICE_TO', $response);
        $this->assertEquals('2013-09-30 23:59:59', $response['INVOICE_TO']);

    }

    /**
     * @group network
     */
    public function testInfo()
    {
        $response = $this->linode->execute(new AccountCommand('info', []));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
        $this->assertArrayHasKey('ACTIVE_SINCE', $response);
        $this->assertArrayHasKey('TRANSFER_POOL', $response);
    }

    /**
     * Mock...
     */
    public function testMockInfo()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('account.info');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'account.info']);

        $this->mock->addResponse($this->getMockPath() . 'account_info.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=account.info', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('ACTIVE_SINCE', $response);
        $this->assertEquals('2011-09-23 15:08:13.0', $response['ACTIVE_SINCE']);

    }

    /**
     *
     */
    public function tearDown()
    {
        Mockery::close();
    }
}
