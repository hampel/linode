<?php namespace Hampel\Linode\Commands;

use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class DomainCommandTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->mock = new Mock();

		$this->client = new Client();
		$this->client->getEmitter()->attach($this->mock);

		$this->linode = new Linode($this->client);
	}

	protected function getMockPath()
	{
		return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
	}

	public function testMissingEmail()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_create_8.json');

		$this->setExpectedException('Hampel\Linode\Exception\LinodeErrorException', 'Error processing Linode command [domain.create]: [8] SOA_Email is required when Type=master');

		$this->linode->execute(new DomainCommand('create', ['domain' => 'mock-domain.com']));
	}

	public function testCreate()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_create.json');

		$response = $this->linode->execute(new DomainCommand('create',
			[
				'domain' => 'mock-domain.com',
				'type' => 'master',
				'soa_email' => 'mock@mock-domain.com'
			]
		));

		$this->assertEquals('?api_action=domain.create&domain=mock-domain.com&type=master&soa_email=mock%40mock-domain.com', $this->linode->getLastQuery());
		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey('DomainID', $response);
		$this->assertEquals(12345, $response['DomainID']);
	}

	public function testUpdate()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_update.json');

		$response = $this->linode->execute(new DomainCommand('update', ['domainid' => 12345]));

		$this->assertEquals('?api_action=domain.update&domainid=12345', $this->linode->getLastQuery());
		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey('DomainID', $response);
		$this->assertEquals(12345, $response['DomainID']);
	}

	public function testDelete()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_delete.json');

		$response = $this->linode->execute(new DomainCommand('delete', ['domainid' => 12345]));

		$this->assertEquals('?api_action=domain.delete&domainid=12345', $this->linode->getLastQuery());
		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey('DomainID', $response);
		$this->assertEquals(12345, $response['DomainID']);
	}

	public function testListSingle()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_list_single.json');

		$response = $this->linode->execute(new DomainCommand('list', ['domainid' => 12345]));

		$this->assertEquals('?api_action=domain.list&domainid=12345', $this->linode->getLastQuery());
		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey(0, $response);
		$this->assertTrue(is_array($response[0]));
		$this->assertArrayHasKey('DOMAINID', $response[0]);
		$this->assertEquals(12345, $response[0]['DOMAINID']);
	}

	public function testList()
	{
		$this->mock->addResponse($this->getMockPath() . 'domain_list_multiple.json');

		$response = $this->linode->execute(new DomainCommand('list'));

		$this->assertEquals('?api_action=domain.list', $this->linode->getLastQuery());
		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertEquals(3, count($response));

		$this->assertEquals(12345, $response[0]['DOMAINID']);
		$this->assertEquals(12346, $response[1]['DOMAINID']);
		$this->assertEquals(12347, $response[2]['DOMAINID']);

		$this->assertEquals('mock-domain.com', $response[0]['DOMAIN']);
		$this->assertEquals('mock-domain2.com', $response[1]['DOMAIN']);
		$this->assertEquals('mock-domain3.com', $response[2]['DOMAIN']);
	}

}
