<?php namespace Hampel\Linode\Commands;

use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class LinodeJobCommandTest extends \PHPUnit_Framework_TestCase
{
    protected $linode;
    protected $linodeid;

    public function setUp()
    {
        date_default_timezone_set('UTC');
        $this->linode = Linode::make(API_KEY);
    }

    /**
     * TODO: this needs to be omitted if we're only testing mocks.
     * @group network
     */
    protected function assertPreConditions()
    {
        $response = $this->linode->execute(new LinodeCommand('list', []));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
        $this->assertGreaterThan(0, count($response));
        $response = array_shift($response);
        $this->assertArrayHasKey('LINODEID', $response);
        $this->linodeid = $response['LINODEID'];
    }

    /**
     * @group network
     */
    public function testJobList()
    {
        // We need a real Linode ID before we can run this
        $response = $this->linode->execute(new LinodeJobCommand('list', ['linodeid' => $this->linodeid]));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
        $this->assertGreaterThan(0, count($response));
    }
}
