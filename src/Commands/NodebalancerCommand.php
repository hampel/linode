<?php namespace Hampel\Linode\Commands;
/**
 * Class NodebalancerCommand
 * Create and manage NodeBalancers, the managed load balancing service (lbass) from Linode, using this family of API calls.
 *
 * @package Hampel\Linode\Commands
 */
class NodebalancerCommand extends Command
{
    /** @var string the command prefix */
    protected $prefix = 'nodebalancer';

    /** @var array allowable actions for $action parameter */
    protected $allowed_actions = ['create', 'delete', 'list','update'];

    /** @var array allowable parameters to create and update calls */
    protected $allowed_parameters = [
        'nodebalancerid', // optional - numeric
        'datacenterid', // required - The DatacenterID from avail.datacenters() where you wish to place this new Linode
        'paymentterm', // optional - One of: 1, 12, or 24
        'label', // required - string This NodeBalancer's label, but this seems to be ignored for the create command (?)
        'clientconnthrottle' // optional - numeric To help mitigate abuse, throttle connections per second, per client IP. 0 to disable. Max of 20.
    ];
}
