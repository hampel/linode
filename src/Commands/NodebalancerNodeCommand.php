<?php namespace Hampel\Linode\Commands;
/**
 * Class NodebalancerCommand
 * Create and manage NodeBalancers, the managed load balancing service (lbass) from Linode, using this family of API calls.
 *
 * @package Hampel\Linode\Commands
 */
class NodebalancerNodeCommand extends Command
{
    /** @var string the command prefix */
    protected $prefix = 'nodebalancer.node';

    /** @var array allowable actions for $action parameter */
    protected $allowed_actions = ['create', 'delete', 'list','update'];

    /** @var array allowable parameters to create and update calls */
    protected $allowed_parameters = [
        'configid', // required - numeric The parent ConfigID to attach this Node to
        'label', // required - numeric This backend Node's label
        'address', // required - The address:port combination used to communicate with this Node
        'weight', // numeric - optional Load balancing weight, 1-255. Higher means more connections.
        'mode' // string - optional The connections mode for this node. One of 'accept', 'reject', or 'drain'
    ];
}
