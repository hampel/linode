<?php namespace Hampel\Linode\Commands;
/**
 * Class NodebalancerCommand
 * Create and manage NodeBalancers, the managed load balancing service (lbass) from Linode, using this family of API calls.
 *
 * @package Hampel\Linode\Commands
 */
class NodebalancerConfigCommand extends Command
{
    /** @var string the command prefix */
    protected $prefix = 'nodebalancer.config';

    /** @var array allowable actions for $action parameter */
    protected $allowed_actions = ['create', 'delete', 'list','update'];

    /** @var array allowable parameters to create and update calls */
    protected $allowed_parameters = [
        'nodebalancerid', // optional - numeric
        'port', // optional - numeric
        'protocol', // optional - string Either 'tcp', 'http', or 'https'
        'algorithm', // optional - string Balancing algorithm. One of 'roundrobin', 'leastconn', 'source'
        'stickiness', // optional - string Session persistence. One of 'none', 'table', 'http_cookie'
        'check', // optional - string Perform active health checks on the backend nodes. One of 'connection', 'http', 'http_body'
        'check_interval', // numeric - optional Seconds between health check probes. 2-3600
        'check_timeout', // numeric - Seconds to wait before considering the probe a failure. 1-30. Must be less than check_interval.
        'check_attempts', // numeric - Number of failed probes before taking a node out of rotation. 1-30
        'check_paths', // string - When check=http, the path to request
        'check_body', // string - When check=http_body, a regex against the expected result body
        'ssl_cert', // string - SSL certificate served by the NodeBalancer when the protocol is 'https'
        'ssl_key', // string - Unpassphrased private key for the SSL certificate when protocol is 'https'
        'configid' // numeric
    ];
}
