<?php namespace Hampel\Linode\Commands;

class UserCommand extends Command
{
	/** @var string the command prefix */
	protected $prefix = 'user';

	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [
		'username', // string (required)
		'password', // string (required)
		'token', // string (optional), required when two-factor authentication is enabled
		'expires', // numeric (optional), number of hours the key will remain valid, between 0 and 8760. 0 means no expiration. Default 168.
		'label', // string (optional), an optional label for this key
	];

	/** @var array allowable actions for $action parameter */
	protected $allowed_actions = ['getapikey'];

}
