<?php namespace Hampel\Linode\Commands;

class DomainCommand extends Command
{
	/** @var string the command prefix */
	protected $prefix = 'domain';

	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [
		"domain", // required - zone's name
		"domainid", // required for update
		"description", // default "" - currently undisplayed
		"type", // required must be master or slave
		"soa_email", // required when type=master
		"refresh_sec", // default 0
		"retry_sec", // default 0
		"expire_sec", // default 0
		"ttl_sec", // default 0
		"status", // default 1 - must be 0, 1 or 2 (disabled, active, edit mode)
		"master_ips", // default "" - when type=slave, the zone's master DNS servers list, semicolon separated
		"axfr_ips" // default "" - IP addresses allowed to AXFR the entire zone, semicolon separated
	];
}
