<?php namespace Hampel\Linode\Commands;

class AvailCommand extends Command
{
    /** @var string the command prefix */
    protected $prefix = 'avail';

    /** @var array allowable actions for $action parameter */
    protected $allowed_actions = ['datacenters','distributions','kernels','linodeplans','nodebalancers','stackscripts'];

    /**
     * Hodgepodge stuff here: different params for different methods
     * @var array allowable parameters to create and update calls
     */
    protected $allowed_parameters = [
        'distributionid', // for avail.distributions & avail.stackscripts
        'kernelid', // avail.kernels
        'isxen', // avail.kernels
        'planid', // avail.linodeplans
        'distributionvendor', // avail.stackscripts
        'distributionidlist',
        'keywords', //
    ];
}
