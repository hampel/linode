<?php namespace Hampel\Linode\Commands;

use Hampel\Linode\Exception\LinodeCommandException;

abstract class Command implements CommandInterface
{
	/** @var string the command prefix (without the dot) */
	protected $prefix;

	/** @var  Action to be executed by the command */
	protected $action;

	/** @var array key-value pairs of options */
	protected $options;

	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [];

	/** @var array allowable actions for $action parameter */
	protected $allowed_actions = ['create', 'list', 'update', 'delete'];

	/**
	 * @param string $action	action for the command, must be one of 'create', 'list', 'update', 'delete'
	 * @param array $options	array of key-value pairs
	 *
	 * @throws LinodeCommandException
	 */
	public function __construct($action, array $options = [])
	{
		$classname = $this->classBassName();

		if (!in_array($action, $this->allowed_actions, true)) throw new LinodeCommandException("Invalid action [{$action}] for [{$classname}]");

		if (!empty($this->allowed_parameters))
		{
			foreach ($options as $key => $value)
			{
				if (!in_array($key, $this->allowed_parameters, true)) throw new LinodeCommandException("Invalid option key [{$key}] for [{$classname}]");
			}
		}

		$this->action = $action;
		$this->options = $options;
	}

	/**
	 * @return string action the action for this command
	 */
	public function getAction()
	{
		return $this->prefix . '.' . $this->action;
	}

	/**
	 * @param string $api_key	api_key for the command
	 *
	 * @return array 			the key-value pairs to send in the query
	 */
	public function build($api_key = '')
	{
		$options['api_action'] = $this->getAction();

		if (!empty($api_key)) $options['api_key'] = $api_key;

		return $options + $this->options;
	}

	protected function classBassName()
	{
		$parts = explode('\\', get_called_class());
		return array_pop($parts);
	}
}
