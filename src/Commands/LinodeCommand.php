<?php namespace Hampel\Linode\Commands;
/**
 * Class LinodeCommand
 * This is what handles creation of Linodes and you will get charged for creating them!
 * There is a 75-linodes-per-hour limiter.
 * @package Hampel\Linode\Commands
 */
class LinodeCommand extends Command
{
	/** @var string the command prefix */
	protected $prefix = 'linode';

    /** @var array allowable actions for $action parameter */
	protected $allowed_actions = ['boot', 'clone', 'create', 'delete', 'list', 'reboot', 'resize', 'shutdown', 'update'];
	
	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [
	    /*
	     * LinodeID
	     * numeric
	     * required in [linode.boot, linode.clone, linode.delete, linode.reboot, linode.resize, linode.shutdown,
	     * 	linode.update]
	     * optional in [linode.list]
	     * The LinodeID to perform this operation on
	     */
		'linodeid',

		/*
		 * ConfigID
		 * numeric
		 * optional in [linode.boot, lionde.reboot]
		 * The ConfigID to boot, available from linode.config.list().
		 */
        'configid',

		/*
		 * DatacenterID
		 * numeric
		 * required in [linode.clone, linode.create]
		 * The DatacenterID from avail.datacenters() where you wish to place this new Linode
		 */
		'datacenterid',

		/*
		 * PlanID
		 * numeric
		 * required in [linode.clone, linode.create, linode.resize]
		 * The desired PlanID available from avail.LinodePlans()
		 */
		'planid',

		/*
		 * PaymentTerm
		 * numeric
		 * optional in [linode.clone, linode.create]
		 * Subscription term in months for prepaid customers. One of: 1, 12, or 24
		 */
		'paymentterm',

		/*
		 * skipChecks
		 * boolean
		 * optional in [linode.delete]
		 * Skips the safety checks and will always delete the Linode
		 */
		'skipchecks',

		/*
		 * Label
		 * string
		 * optional in [linode.update]
		 * This Linode's label
		 */
		'label',

		/*
		 * lpm_displayGroup
		 * string
		 * optional in [linode.update]
		 * Display group in the Linode list inside the Linode Manager
		 */
		'lpm_displaygroup',

		/*
		 * Alert_cpu_enabled
		 * boolean
		 * optional in [linode.update]
		 * Enable the cpu usage email alert
		 */
		'alert_cpu_enabled',

		/*
		 * Alert_cpu_threshold
		 * numeric
		 * optional in [linode.update]
		 * CPU Alert threshold, percentage 0-800
		 */
		'alert_cpu_threshold',

		/*
		 * Alert_diskio_enabled
		 * boolean
		 * optional in [linode.update]
		 * Enable the disk IO email alert
		 */
		'alert_diskio_enabled',

		/*
		 * Alert_diskio_threshold
		 * numeric
		 * optional in [linode.update]
		 * IO ops/sec
		 */
		'alert_diskio_threshold',

		/*
		 * Alert_bwin_enabled
		 * boolean
		 * optional in [linode.update]
		 * Enable the incoming bandwidth email alert
		 */
		'alert_bwin_enabled',

		/*
		 * Alert_bwin_threshold
		 * numeric
		 * optional in [linode.update]
		 * Mb/sec
		 */
		'alert_bwin_threshold',

		/*
		 * Alert_bwout_enabled
		 * boolean
		 * optional in [linode.update]
		 * Enable the outgoing bandwidth email alert
		 */
		'alert_bwout_enabled',

		/*
		 * Alert_bwout_threshold
		 * numeric
		 * optional in [linode.update]
		 * Mb/sec
		 */
		'alert_bwout_threshold',

		/*
		 * Alert_bwquota_enabled
		 * boolean
		 * optional in [linode.update]
		 * Enable the bw quote email alert
		 */
		'alert_bwquota_enabled',

		/*
		 * Alert_bwquota_threshold
		 * numeric
		 * optional in [linode.update]
		 * Percentage of monthly bw quota
		 */
		'alert_bwquota_threshold',

		/*
		 * backupWindow
		 * numeric
		 * optional in [linode.update]
		 */
		'backupwindow',

		/*
		 * backupWeeklyDay
		 * numeric
		 * optional in [linode.update]
		 */
		'backupweeklyday',

		/*
		 * watchdog
		 * boolean
		 * optional in [linode.update]
		 * Enable the Lassie shutdown watchdog
		 */
		'watchdog',

		/*
		 * ms_ssh_disabled
		 * boolean
		 * optional in [linode.update]
		 */
		'ms_ssh_disabled',

		/*
		 * ms_ssh_user
		 * string
		 * optional in [linode.update]
		 */
		'ms_ssh_user',

		/*
		 * ms_ssh_ip
		 * string
		 * optional in [linode.update]
		 */
		'ms_ssh_ip',

		/*
		 * ms_ssh_port
		 * numeric
		 * optional in [linode.update]
		 */
		'ms_ssh_port',
	];
}
