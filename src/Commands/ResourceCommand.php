<?php namespace Hampel\Linode\Commands;

class ResourceCommand extends Command
{
	/** @var string the command prefix */
	protected $prefix = 'domain.resource';

	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [
		"domainid", // required
		"resourceid", // required for update
		"type", // required one of NS, MX, A, AAAA, CNAME, TXT, SRV
		"name", // default "" - hostname of FQDN. When type=MX the subdomain to delegate to the target MX server
		"target", 	// default ""
		// when type=MX the hostname
		// when type=CNAME the target of the alias
		// when type=TXT the value of the record
		// when type=A or AAAA the token of [remote_addr] will be substituted with the IP address of the request
		"priority", // default 10
		"weight", // default 5,
		"port", // default 80,
		"protocol", // default 'udp' - the protocol to append to an SRV record. Ignored on other record types
		"ttl_sec" // default 0 - TTL. Leave as 0 to accept Linode default
	];
}
